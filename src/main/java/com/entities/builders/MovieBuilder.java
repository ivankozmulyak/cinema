package com.entities.builders;

import com.entities.Genre;
import com.entities.Movie;

import java.util.List;

public class MovieBuilder {

    private String title;
    private int duration;
    private List<Genre> genres;
    private Float rating;

    public MovieBuilder title(String title) {
        this.title = title;
        return this;
    }

    public MovieBuilder duration(int duration) {
        this.duration = duration;
        return this;
    }

    public MovieBuilder genres(List<Genre> genres) {
        this.genres = genres;
        return this;
    }

    public MovieBuilder rating(Float rating) {
        this.rating = rating;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public int getDuration() {
        return duration;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public Float getRating() {
        return rating;
    }

    public Movie build(){
        return new Movie(this);
    }
}
