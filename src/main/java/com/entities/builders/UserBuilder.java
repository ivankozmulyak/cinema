package com.entities.builders;

import com.entities.User;

public class UserBuilder {

    private String name;

    private String phone;

    public UserBuilder name(String name){
        this.name = name;
        return this;
    }

    public UserBuilder phone(String phone){
        this.phone = phone;
        return this;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public User build(){
        return new User(this);
    }
}
