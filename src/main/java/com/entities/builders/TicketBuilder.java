package com.entities.builders;

import com.entities.Movie;
import com.entities.Ticket;
import com.entities.User;

import java.time.LocalDateTime;

public class TicketBuilder {

    private Movie movie;
    private User user;
    private LocalDateTime date;
    private Float price;

    public TicketBuilder movie(Movie movie){
        this.movie = movie;
        return this;
    }

    public TicketBuilder user(User user){
        this.user = user;
        return this;
    }

    public TicketBuilder date(LocalDateTime date){
        this.date = date;
        return this;
    }

    public TicketBuilder price(Float price){
        this.price = price;
        return this;
    }

    public Movie getMovie() {
        return movie;
    }

    public User getUser() {
        return user;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public Float getPrice() {
        return price;
    }

    public Ticket build(){
        return new Ticket( this);
    }

}
