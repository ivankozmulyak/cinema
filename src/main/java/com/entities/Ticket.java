package com.entities;

import com.entities.builders.TicketBuilder;
import com.utilities.validators.ObjectValidator;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

public class Ticket {
    private UUID id = UUID.randomUUID();

    @NotNull(message = "Movie cannot be null")
    private Movie movie;

    @NotNull(message = "User cannot be null")
    private User user;

    @NotNull(message = "Date cannot be null")
    private LocalDateTime date;

    @NotNull(message = "Price cannot be null")
    private Float price;

    public Ticket(TicketBuilder ticketBuilder) {
        ObjectValidator<Ticket> validator = new ObjectValidator<>(this);
        this.movie = ticketBuilder.getMovie();
        this.user = ticketBuilder.getUser();
        this.date = ticketBuilder.getDate();
        this.price = ticketBuilder.getPrice();
        validator.runValidation();
    }

    public Ticket() {
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public Movie getMovie() {
        return movie;
    }

    public User getUser() {
        return user;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public Float getPrice() {
        return price;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ticket ticket = (Ticket) o;
        return id.equals(ticket.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
