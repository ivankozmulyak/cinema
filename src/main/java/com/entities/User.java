package com.entities;

import com.entities.builders.UserBuilder;
import com.utilities.validators.ObjectValidator;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Objects;
import java.util.UUID;

public class User {
    private UUID  id = UUID.randomUUID();

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    @Size(min = 1, message = "Name cannot be empty")
    private String name;

    @Pattern(regexp = "^((\\+38)(\\d{10})$)", message = "Not correct phone number")
    private String phone;

    public User(UserBuilder userBuilder) {
        ObjectValidator<User> validator = new ObjectValidator<>(this);
        this.name = userBuilder.getName();
        this.phone = userBuilder.getPhone();
        validator.runValidation();
    }

    public User() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(phone, user.phone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(phone);
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}
