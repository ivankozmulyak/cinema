package com.entities;

public enum Genre {
  ACTION, COMEDY, HORROR, ROMANCE, THRILLER
}
