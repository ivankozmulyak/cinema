package com.entities;

import com.entities.builders.MovieBuilder;
import com.utilities.validators.ObjectValidator;
import lombok.Getter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class Movie {

    public void setId(UUID id) {
        this.id = id;
    }

    private UUID id = UUID.randomUUID();

    @NotNull(message = "Title cannot be null")
    @Size(min = 1, message = "Title cannot be empty")
    private String title;

    @NotNull(message = "Duration cannot be null")
    private int duration;

    private List<Genre> genres;

    @Min(value = 0,message = "The minimum value must be greater than zero")
    @Max(value = 10, message = "The maximal value must be less than 10")
    private Float rating;

    public Movie(MovieBuilder movieBuilder) {
        ObjectValidator<Movie> validator = new ObjectValidator<>(this);
        this.title = movieBuilder.getTitle();
        this.duration = movieBuilder.getDuration();
        this.genres = movieBuilder.getGenres();
        this.rating = movieBuilder.getRating();
        validator.runValidation();
    }

    public UUID getId() {
        return id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public String getTitle() {
        return title;
    }

    public int getDuration() {
        return duration;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public Float getRating() {
        return rating;
    }


    public String[] getStringsGenres() {
        if (genres != null) {
            return Arrays.stream(genres.toArray())
                    .map(Object::toString)
                    .toArray(String[]::new);
        } else {
            return null;
        }
    }

    public Movie() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Movie movie = (Movie) o;
        return title.equals(movie.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title);
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", duration=" + duration +
                ", genres=" + genres +
                ", rating=" + rating +
                '}';
    }
}
