package com.utilities.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class FileRepository<T> {

    private final File file;
    ObjectMapper mapper = new ObjectMapper();
    Class<T> tClass;

    public FileRepository(File file, Class<T> tClass) {
        this.file = file;
        this.tClass = tClass;
        editProperties();
    }

    private void editProperties(){
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(
                SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        mapper.disable(
                SerializationFeature.WRITE_DURATIONS_AS_TIMESTAMPS);
    }


    public List<T> getDataFromFile() throws IOException {
        if (file.length() == 0){
            return new LinkedList<>();
        }
        CollectionType listType = mapper.getTypeFactory().constructCollectionType(ArrayList.class, tClass);
        return mapper.readValue(file, listType);
    }

    public void saveDataToFile(List<T> objects) throws IOException {
        mapper.writeValue(file, objects);
    }
}
