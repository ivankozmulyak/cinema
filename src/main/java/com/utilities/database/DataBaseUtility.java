package com.utilities.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DataBaseUtility {

    private DataBaseUtility() {
    }

    public static Connection initConnectionWithTables(Connection connection) throws SQLException {
        String userTable = "CREATE MEMORY TABLE users ( id uuid NOT NULL, name varchar(30) NOT NULL,  phone varchar(20) unique NOT NULL, PRIMARY KEY (id))";
    String movieTable = "CREATE MEMORY TABLE movies (" +
            "    id uuid NOT NULL," +
            "    title varchar(30) unique NOT NULL," +
            "    duration integer," +
            "    genres array," +
            "    rating double," +
            "    PRIMARY KEY (id)" +
            ")";
    String ticketTable = "CREATE MEMORY TABLE tickets (" +
            "    id uuid," +
            "    movie uuid," +
            "    username uuid," +
            "    price double," +
            "    date timestamp," +
            "    PRIMARY KEY (id)," +
            "    FOREIGN KEY (movie)" +
            "        REFERENCES movies(id)," +
            "    FOREIGN KEY (username)" +
            "        REFERENCES users(id)" +
            ")";

        try ( PreparedStatement createUsersStatement = connection.prepareStatement(userTable);
    PreparedStatement createMoviesStatement = connection.prepareStatement(movieTable);
    PreparedStatement createTicketsStatement = connection.prepareStatement(ticketTable)) {
        createUsersStatement.execute();
        createMoviesStatement.execute();
        createTicketsStatement.execute();
    }
        return connection;
}

}
