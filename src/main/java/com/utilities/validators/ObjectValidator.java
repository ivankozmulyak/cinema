package com.utilities.validators;

import com.exceptions.WrongParameterException;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import java.util.Set;

public class ObjectValidator<T> {

    private T object;

    public ObjectValidator(T object) {
        this.object = object;
    }

    public void runValidation(){
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        javax.validation.Validator validator = validatorFactory.getValidator();
        Set<ConstraintViolation<T>> violations = validator.validate(object);

        StringBuilder errors = new StringBuilder();

        for (ConstraintViolation<T> violation : violations) {
            errors.append("\n").append(violation.getPropertyPath()).append(" - ").append(violation.getMessage());
        }

        if (errors.length() != 0) throw new WrongParameterException(errors.toString());
    }
}
