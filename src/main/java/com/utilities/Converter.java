package com.utilities;

import com.entities.Genre;

import java.util.ArrayList;
import java.util.List;

public class Converter {

    public List<Genre> convertStringToGenres(String[] genresInString) {
        List<Genre> genres = new ArrayList<>();
        for (String genreInString: genresInString) {
            genres.add(Genre.valueOf(genreInString.toUpperCase()));
        }
        return genres;
    }
}
