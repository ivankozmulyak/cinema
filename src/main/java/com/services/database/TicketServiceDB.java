package com.services.database;

import com.entities.Movie;
import com.entities.Ticket;
import com.entities.User;
import com.repositories.database.TicketRepositoryDataBase;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public class TicketServiceDB {
    TicketRepositoryDataBase ticketRepository;

    public TicketServiceDB() throws SQLException {
        ticketRepository = new TicketRepositoryDataBase();
    }

    public TicketRepositoryDataBase getTicketRepository() {
        return ticketRepository;
    }

    public boolean add(Ticket ticket) throws SQLException {
        return ticketRepository.add(ticket);
    }

    public List<Ticket> getAll() throws SQLException {
        return ticketRepository.getAll();
    }

    public Ticket findById(UUID id) throws FileNotFoundException, SQLException {
        return ticketRepository.findById(id);
    }

    public boolean deleteById(UUID id) throws SQLException {
        return ticketRepository.deleteById(id);
    }

    public boolean update(Ticket ticket) throws SQLException {
        return ticketRepository.update(ticket);
    }

    public void deleteAll() throws SQLException {
        ticketRepository.deleteAll();
    }

    public List<User> getUsersByDate(LocalDateTime date) throws SQLException {
        return ticketRepository.getUsersByDate(date);
    }

    public List<Movie> getMoviesByCurrentDate() throws SQLException {
        return ticketRepository.getMoviesByCurrentDate();
    }

    public List<Movie> getMoviesByDate(LocalDate localDate) throws SQLException {
        return ticketRepository.getMoviesByDate(localDate);
    }

    public float getSumByDateOfMonth(LocalDateTime date) throws SQLException {
        return ticketRepository.getSumByDateOfMonth(date);
    }

    public List<Movie> getSortedMoviesByCountOfTickets() throws SQLException {
        return ticketRepository.getSortedMoviesByCountOfTickets();
    }

}
