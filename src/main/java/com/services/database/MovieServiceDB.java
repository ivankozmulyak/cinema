package com.services.database;

import com.entities.Movie;
import com.repositories.database.MovieRepositoryDataBase;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public class MovieServiceDB {
    private final MovieRepositoryDataBase movieRepository;

    public MovieServiceDB() throws SQLException {
        movieRepository = new MovieRepositoryDataBase();
    }

    public MovieServiceDB(Connection connection) {
        movieRepository = new MovieRepositoryDataBase(connection);
    }

    public MovieRepositoryDataBase getMovieRepository() {
        return movieRepository;
    }

    public void deleteAll() throws SQLException {
        movieRepository.deleteAll();
    }

    public boolean deleteById(UUID id) throws SQLException {
        return movieRepository.deleteById(id);
    }

    public boolean update(Movie movie) throws SQLException {
        return movieRepository.update(movie);
    }

    public List<Movie> getAll() throws SQLException {
        return movieRepository.getAll();
    }

    public boolean add(Movie movie) throws SQLException {
        return movieRepository.add(movie);
    }


    public Movie findById(UUID id) throws FileNotFoundException, SQLException {
        return movieRepository.findById(id);
    }
}
