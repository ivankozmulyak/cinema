package com.services.database;

import com.entities.User;
import com.repositories.database.UserRepositoryDataBase;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public class UserServiceDB {
    public UserRepositoryDataBase getUserRepository() {
        return userRepository;
    }

    private final UserRepositoryDataBase userRepository;

    public UserServiceDB() throws SQLException {
        userRepository = new UserRepositoryDataBase();
    }

    public List<User> getAll() throws SQLException {
        return userRepository.getAll();
    }

    public boolean add(User user) throws SQLException {
        return userRepository.add(user);
    }

    public boolean update(User user) throws SQLException {
        return userRepository.update(user);
    }
    public void deleteAll() throws SQLException {
        userRepository.deleteAll();
    }

    public User findByPhone(String phone) throws FileNotFoundException, SQLException {
        return userRepository.findByPhone(phone);
    }

    public User findById(UUID id) throws FileNotFoundException, SQLException {
        return userRepository.findById(id);
    }

    public boolean deleteById(UUID id) throws SQLException {
        return userRepository.deleteById(id);
    }



}
