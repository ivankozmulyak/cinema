package com.services.json;

import com.entities.Movie;
import com.repositories.json.MovieRepositoryImpl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

public class MovieService {

    private MovieRepositoryImpl movieRepositoryImpl;

    public MovieService(File file) throws IOException {
        movieRepositoryImpl = new MovieRepositoryImpl(file);
    }

    public MovieRepositoryImpl getMovieRepository() {
        return movieRepositoryImpl;
    }

    public boolean deleteByTitle(String title) {
        return movieRepositoryImpl.deleteByTitle(title);
    }

    public boolean deleteById(UUID id) {
        return movieRepositoryImpl.deleteById(id);
    }

    public boolean update(UUID id, Movie movie) {
        return movieRepositoryImpl.update(id, movie);
    }

    public List<Movie> getAllMovies() {
        return movieRepositoryImpl.getAllMovies();
    }

    public boolean addMovie(Movie movie) {
        return movieRepositoryImpl.add(movie);
    }

    public Movie findByTitle(String title) throws FileNotFoundException {
       return movieRepositoryImpl.findByTitle(title);
    }

    public Movie findById(UUID id) throws FileNotFoundException {
       return movieRepositoryImpl.findById(id);
    }

    public void saveDataToFile() throws IOException {
        movieRepositoryImpl.saveDataToFile();
    }

}
