package com.services.json;

import com.entities.User;
import com.repositories.json.UserRepositoryImpl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

public class UserService {

    public UserRepositoryImpl getUserRepository() {
        return userRepository;
    }

    private final UserRepositoryImpl userRepository;

    public UserService(File file) throws IOException {
        userRepository = new UserRepositoryImpl(file);
    }

    public List<User> getUsers() {
        return userRepository.getUsers();
    }

    public boolean addUser(User user) {
        return userRepository.add(user);
    }

    public boolean update(UUID id, User user) {
        return userRepository.update(id, user);
    }


    public User findByPhone(String phone) throws FileNotFoundException {
        return userRepository.findByPhone(phone);
    }

    public User findById(UUID id) throws FileNotFoundException {
        return userRepository.findById(id);
    }


    public void saveDataToFile() throws IOException {
        userRepository.saveDataToFile();
    }

    public boolean deleteByPhone(String phone) {
        return userRepository.deleteByPhone(phone);
    }

    public boolean deleteById(UUID id) {
        return userRepository.deleteById(id);
    }



}
