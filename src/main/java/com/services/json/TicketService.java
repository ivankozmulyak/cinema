package com.services.json;

import com.entities.Movie;
import com.entities.Ticket;
import com.entities.User;
import com.repositories.json.TicketRepositoryImpl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

import static java.util.stream.Collectors.*;

public class TicketService {

    TicketRepositoryImpl ticketRepository;

    public TicketService(File file) throws IOException {
        ticketRepository = new TicketRepositoryImpl(file);
    }

    public TicketRepositoryImpl getTicketRepository() {
        return ticketRepository;
    }

    public boolean addTicket(Ticket ticket){
        return ticketRepository.add(ticket);
    }

    public List<Ticket> getTickets(){
        return ticketRepository.getTickets();
    }

    public Ticket findById(UUID id) throws FileNotFoundException {
        return ticketRepository.findById(id);
    }

    public boolean deleteById(UUID id) {
        return ticketRepository.deleteById(id);
    }

    public boolean update(UUID id, Ticket ticket) {
        return ticketRepository.update(id, ticket);
    }


    public void saveDataToFile() throws IOException {
        ticketRepository.saveDataToFile();
    }

    public Map<Movie, Long> getMovieWithRating(){
        return getTickets().stream().collect(groupingBy(Ticket::getMovie, counting()));
    }

    public Map<Movie, Long> sortByCountVisits(Long threshold){
        Map<Movie, Long> movies = new HashMap<>();
        getMovieWithRating().forEach((movie, count) -> {if (count <= threshold) movies.put(movie, count); });
        return movies;
    }

    public Set<Movie> getTodayFilms()  {
        return getTickets().stream() .filter(ticket -> isTicketOnDate(ticket, LocalDate.now()))
                                        .map(Ticket::getMovie)
                                        .collect(toSet());
    }

    public Set<User> getUsersByMovieAndDate(Movie movie, LocalDateTime date){
        return getTickets().stream() .filter(ticket -> ticket.getDate().equals(date) && ticket.getMovie().equals(movie))
                                        .map(Ticket::getUser)
                                        .collect(toSet());
    }

    public boolean isTicketOnDate(Ticket ticket, LocalDate date){
        return ticket.getDate().toLocalDate().equals(date);

    }

    public Float getMoneyForOneMonth(int year, int month){
        return getTickets().stream()
                .filter(ticket ->   ticket.getDate().getMonth().getValue() == month &&
                                    ticket.getDate().getYear() == year)
                .map(Ticket::getPrice)
                .reduce(0.0F, Float::sum);
    }


}
