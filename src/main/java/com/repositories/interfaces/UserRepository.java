package com.repositories.interfaces;

import com.entities.User;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.UUID;

public interface UserRepository {

    User findById(UUID id) throws FileNotFoundException, SQLException;
    User findByPhone(String phone) throws FileNotFoundException, SQLException;
    boolean deleteById(UUID id) throws SQLException;
    boolean update(User user) throws SQLException;
    boolean add(User user) throws SQLException;
}
