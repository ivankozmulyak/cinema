package com.repositories.interfaces;

import com.entities.Ticket;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.UUID;

public interface TicketRepository {

    Ticket findById(UUID id) throws FileNotFoundException, SQLException;
    boolean deleteById(UUID id) throws SQLException;
    boolean update(Ticket ticket) throws SQLException;
    boolean add(Ticket ticket) throws SQLException;
}
