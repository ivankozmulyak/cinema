package com.repositories.interfaces;

import com.entities.Movie;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.UUID;

public interface MovieRepository {
    boolean deleteById(UUID id) throws SQLException;
    boolean update(Movie movie) throws SQLException;
    boolean add(Movie movie) throws SQLException;
    Movie findById(UUID id) throws FileNotFoundException, SQLException;
}
