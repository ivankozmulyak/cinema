package com.repositories.database;

import com.entities.*;
import com.repositories.interfaces.TicketRepository;
import com.utilities.Converter;
import com.utilities.database.DataBaseConnector;

import java.io.FileNotFoundException;
import java.sql.*;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.UUID;

public class TicketRepositoryDataBase implements TicketRepository {

    private final Connection connection;

    public TicketRepositoryDataBase() throws SQLException {
        connection = DataBaseConnector.getConnection();
    }

    public TicketRepositoryDataBase(Connection connection) {
        this.connection = connection;
    }


    public List<Ticket> getAll() throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(TicketRepositoryDataBase.SQLTicket.GET_ALL.QUERY)) {
            List<Ticket> tickets = new ArrayList<>();
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Ticket ticket = buildTicket(rs);
                tickets.add(ticket);
            }
            return tickets;
        }
    }

    private Ticket buildTicket(ResultSet rs) throws SQLException {
        User user = new User();
        Movie movie = new Movie();
        Ticket ticket = new Ticket();
        user.setId(UUID.fromString(rs.getString("user_id")));
        user.setName(rs.getString("user_name"));
        user.setPhone(rs.getString("user_phone"));
        movie.setId(UUID.fromString(rs.getString("movie_id")));
        movie.setTitle(rs.getString("movie_title"));
        movie.setDuration(rs.getInt("movie_duration"));
        movie.setGenres(new Converter().convertStringToGenres((String[]) rs.getArray("movie_genres").getArray()));
        movie.setRating(rs.getFloat("movie_rating"));
        ticket.setId(UUID.fromString(rs.getString("ticket_id")));
        ticket.setDate(rs.getTimestamp("ticket_date").toLocalDateTime());
        ticket.setPrice(rs.getFloat("ticket_price"));
        ticket.setUser(user);
        ticket.setMovie(movie);
        return ticket;


    }

    @Override
    public Ticket findById(UUID id) throws FileNotFoundException, SQLException {
        Ticket ticket;
        try (PreparedStatement statement = connection.prepareStatement(TicketRepositoryDataBase.SQLTicket.GET.QUERY)) {
            statement.setObject(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                ticket = buildTicket(rs);
                ticket.setId(UUID.fromString(rs.getString(1)));
                return ticket;
            } else {
                throw new FileNotFoundException("User with id \"" + id + "\" not found");
            }
        }
    }

    @Override
    public boolean deleteById(UUID id) throws SQLException {
        boolean result;
        try (PreparedStatement statement = connection.prepareStatement(SQLTicket.DELETE.QUERY)) {
            statement.setObject(1, id);
            result = statement.executeQuery().next();
        }
        return result;
    }

    @Override
    public boolean add(Ticket ticket) throws SQLException {
        boolean result;
        try (PreparedStatement statement = connection.prepareStatement(SQLTicket.INSERT.QUERY)) {
            statement.setObject(1, ticket.getId());
            statement.setFloat(4, ticket.getPrice());
            statement.setObject(5, ticket.getDate());
            statement.setObject(2, ticket.getUser().getId());
            statement.setObject(3, ticket.getMovie().getId());
            result = statement.executeQuery().next();
        }
        return result;
    }


    public void deleteAll() throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(SQLTicket.DELETE_ALL.QUERY)) {
            statement.executeUpdate();
        }
    }



    @Override
    public boolean update(Ticket ticket) throws SQLException {
        boolean result;
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQLTicket.UPDATE.QUERY)) {
            preparedStatement.setFloat(3, ticket.getPrice());
            preparedStatement.setTimestamp(4, Timestamp.valueOf(ticket.getDate()));
            preparedStatement.setObject(1, ticket.getUser().getId());
            preparedStatement.setObject(2, ticket.getMovie().getId());
            preparedStatement.setObject(5, ticket.getId());
            result = preparedStatement.executeQuery().next();
        }
        return result;
    }

    public List<User> getUsersByDate(LocalDateTime date) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQLTicket.GET_USERS_BY_DATE.QUERY)) {
            List<User> users = new ArrayList<>();
            preparedStatement.setDate(1, Date.valueOf(date.toLocalDate()));
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                User user = getUserFromResultSet(rs);
                users.add(user);
            }
            return users;
        }
    }


    private Movie getMovieFromResultSet(ResultSet rs) throws SQLException {
        Movie movie = new Movie();
        movie.setId(UUID.fromString(rs.getString("movie_id")));
        movie.setTitle(rs.getString("movie_title"));
        movie.setDuration(rs.getInt("movie_duration"));
        movie.setGenres(new Converter().convertStringToGenres((String[]) rs.getArray("movie_genres").getArray()));
        movie.setRating(rs.getFloat("movie_rating"));
        return movie;
    }

    private User getUserFromResultSet(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId(UUID.fromString(rs.getString("user_id")));
        user.setName(rs.getString("user_name"));
        user.setPhone(rs.getString("user_phone"));
        return user;
    }


    public List<Movie> getMoviesByCurrentDate() throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQLTicket.GET_MOVIES_BY_DATE.QUERY)) {
            List<Movie> movies = new ArrayList<>();
            preparedStatement.setDate(1, Date.valueOf(LocalDate.now()));
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Movie movie = getMovieFromResultSet(rs);
                movies.add(movie);
            }
            return movies;
        }
    }
    public List<Movie> getMoviesByDate(LocalDate localDate) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQLTicket.GET_MOVIES_BY_DATE.QUERY)) {
            List<Movie> movies = new ArrayList<>();
            preparedStatement.setDate(1, Date.valueOf(localDate));
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Movie movie = getMovieFromResultSet(rs);
                movies.add(movie);
            }
            return movies;
        }
    }


    public float getSumByDateOfMonth(LocalDateTime date) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQLTicket.GET_MONEY_FOR_MONTH.QUERY)) {
            preparedStatement.setDate(1, Date.valueOf(date.toLocalDate()));
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                return rs.getFloat("sum");
            }
            return 0;
        }
    }

    public List<Movie> getSortedMoviesByCountOfTickets() throws SQLException {
        try (Statement statement = connection.createStatement()) {
            List<Movie> movies = new ArrayList<>();
            ResultSet rs = statement.executeQuery(SQLTicket.GET_SORTED_MOVIES.QUERY);
            while (rs.next()) {
                Movie movie = getMovieFromResultSet(rs);
                movies.add(movie);
            }
            return movies;
        }
    }


    enum SQLTicket {
        GET("SELECT t.id AS ticket_id, t.date AS ticket_date , t.price AS ticket_price, u.id AS user_id, u.name AS user_name, u.phone AS user_phone, m.id AS movie_id, m.title AS movie_title, m.duration AS movie_duration, m.genres AS movie_genres, m.rating AS movie_rating FROM tickets AS t INNER JOIN users AS u ON u.id = t.username INNER JOIN movies AS m ON t.movie = m.id" +
                " WHERE t.id = (?)"),
        GET_ALL("SELECT t.id AS ticket_id, t.date AS ticket_date , t.price AS ticket_price, u.id AS user_id, u.name AS user_name, u.phone AS user_phone, m.id AS movie_id, m.title AS movie_title, m.duration AS movie_duration, m.genres AS movie_genres, m.rating AS movie_rating FROM tickets AS t INNER JOIN users AS u ON u.id = t.username INNER JOIN movies AS m ON t.movie = m.id"),
        INSERT("INSERT INTO tickets (id, username, movie, price, date) VALUES ((?), (?), (?), (?), (?)) RETURNING id"),
        DELETE("DELETE FROM tickets WHERE id = (?) RETURNING id"),
        DELETE_ALL("DELETE FROM tickets"),
        UPDATE("UPDATE tickets SET username = (?), movie = (?), price = (?), date = (?) WHERE id = (?) RETURNING id"),
        GET_USERS_BY_DATE("SELECT DISTINCT u.id AS user_id, u.name AS user_name, u.phone AS user_phone " +
                "FROM tickets AS t " +
                "INNER JOIN users AS u ON u.id = t.username " +
                "WHERE DATE(t.date) = (?)"),
        GET_MOVIES_BY_DATE("SELECT DISTINCT m.id AS movie_id, m.title AS movie_title, m.duration AS movie_duration, m.genres AS movie_genres, m.rating AS movie_rating " +
                "FROM tickets AS t " +
                "INNER JOIN movies AS m ON m.id = t.movie " +
                "WHERE DATE(t.date) = (?) ORDER BY title"),
        GET_MONEY_FOR_MONTH("SELECT DISTINCT SUM(t.price) as sum " +
                "FROM tickets AS t " +
                "WHERE date_trunc('month', t.date::date) = date_trunc('month', ?::date);"),
        GET_SORTED_MOVIES("SELECT m.id AS movie_id, m.title AS movie_title, m.duration AS movie_duration, m.genres AS movie_genres, m.rating AS movie_rating, count(*) AS count " +
                "FROM tickets AS t " +
                "INNER JOIN movies AS m ON t.movie = m.id " +
                "GROUP BY m.id, m.title " +
                "ORDER BY count DESC, m.title");
        String QUERY;
        SQLTicket(String QUERY) {
            this.QUERY = QUERY;
        }
    }
}
