package com.repositories.database;

import com.entities.Movie;
import com.repositories.interfaces.MovieRepository;
import com.utilities.Converter;
import com.utilities.database.DataBaseConnector;

import javax.validation.constraints.NotNull;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MovieRepositoryDataBase implements MovieRepository {

    @NotNull
    private final Connection connection;

    public MovieRepositoryDataBase(Connection connection) {
        this.connection = connection;
    }

    public MovieRepositoryDataBase() throws SQLException {
        connection = DataBaseConnector.getConnection();
    }

    public List<Movie> getAll() throws SQLException {
        List<Movie> usersFromDataBase = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(SQLMovie.GET_ALL.QUERY)) {
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Movie movie = getMovieFromResultSet(rs);
                movie.setId(UUID.fromString(rs.getString(1)));
                usersFromDataBase.add(movie);
            }
        }
        return usersFromDataBase;
    }

    @Override
    public boolean deleteById(UUID id) throws SQLException {
        boolean result;

        try (PreparedStatement statement = connection.prepareStatement(SQLMovie.DELETE_BY_ID.QUERY)) {
            statement.setObject(1, id);
            result = statement.executeQuery().next();
        }
        return result;
    }

    @Override
    public boolean update(Movie movie) throws SQLException {
        boolean result;

        try (PreparedStatement statement = connection.prepareStatement(SQLMovie.UPDATE.QUERY)) {
            statement.setString(1, movie.getTitle());
            statement.setInt(2, movie.getDuration());
            statement.setArray(3, connection.createArrayOf("text", movie.getStringsGenres()));
            statement.setFloat(4, movie.getRating());
            statement.setObject(5, movie.getId());
            result = statement.executeQuery().next();
        }
        return result;
    }

    @Override
    public boolean add(Movie movie) throws SQLException {
        boolean result;
        try (PreparedStatement statement = connection.prepareStatement(SQLMovie.INSERT.QUERY)) {
            statement.setObject(1, movie.getId());
            statement.setString(2, movie.getTitle());
            statement.setInt(3, movie.getDuration());
            statement.setArray(4, connection.createArrayOf("text", movie.getStringsGenres()));
            statement.setFloat(5, movie.getRating());
            result = statement.executeQuery().next();
        }
        return result;
    }


    @Override
    public Movie findById(UUID id) throws FileNotFoundException, SQLException {
        Movie movie;
        try (PreparedStatement statement = connection.prepareStatement(SQLMovie.GET.QUERY)) {
            statement.setObject(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                movie = getMovieFromResultSet(rs);
                movie.setId(UUID.fromString(rs.getString(1)));
                return movie;
            } else {
                throw new FileNotFoundException("User with id \"" + id + "\" not found");
            }
        }

    }

    public void deleteAll() throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(SQLMovie.DELETE_ALL.QUERY)) {
            statement.executeUpdate();
        }
    }

    private Movie getMovieFromResultSet(ResultSet rs) throws SQLException {
        Movie movie = new Movie();
        movie.setId(UUID.fromString(rs.getString("id")));
        movie.setTitle(rs.getString("title"));
        movie.setDuration(rs.getInt("duration"));
        movie.setGenres(new Converter().convertStringToGenres((String[]) rs.getArray("genres").getArray()));
        movie.setRating(rs.getFloat("rating"));
        return movie;
    }

    enum SQLMovie {
        GET("SELECT id, title, duration, genres, rating FROM movies WHERE id = (?)"),
        GET_ALL("SELECT * FROM movies"),
        DELETE_ALL("DELETE FROM movies"),
        INSERT("INSERT INTO movies (id, title, duration, genres, rating) VALUES ((?), (?), (?), (?), (?)) RETURNING id"),
        DELETE_BY_ID("DELETE FROM movies WHERE id = (?) RETURNING id"),
        UPDATE("UPDATE movies SET title = (?), duration = (?), genres = (?), rating = (?) WHERE id = (?) RETURNING id");

        String QUERY;

        SQLMovie(String QUERY) {
            this.QUERY = QUERY;
        }
    }
}
