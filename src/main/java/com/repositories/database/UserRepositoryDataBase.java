package com.repositories.database;

import com.entities.User;
import com.repositories.interfaces.UserRepository;
import com.utilities.database.DataBaseConnector;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserRepositoryDataBase implements UserRepository {

    private final Connection connection;

    public UserRepositoryDataBase(Connection connection) {
        this.connection = connection;
    }

    public UserRepositoryDataBase() throws SQLException {
        this.connection = DataBaseConnector.getConnection();
    }

    public List<User> getAll() throws SQLException {
        List<User> usersFromDataBase = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(SQLUser.GET_ALL.QUERY)) {
            ResultSet rs = statement.executeQuery();
            while (rs.next()){
                User user = getUserFromResultSet(rs);
                user.setId(UUID.fromString(rs.getString(1)));
                usersFromDataBase.add(user);
            }
        }
        return usersFromDataBase;
    }

    @Override
    public boolean add(User user) throws SQLException {
        boolean result;

        try (PreparedStatement statement = connection.prepareStatement(SQLUser.INSERT.QUERY)) {
            statement.setObject(1, user.getId());
            statement.setString(2, user.getName());
            statement.setString(3,user.getPhone());
            result = statement.executeQuery().next();
        }
        return result;
    }

    public void deleteAll() throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(SQLUser.DELETE_ALL.QUERY)) {
            statement.executeQuery();
        }
    }

    @Override
    public User findById(UUID id) throws FileNotFoundException, SQLException {
        User user;
        try (PreparedStatement statement = connection.prepareStatement(SQLUser.GET_BY_ID.QUERY)) {
            statement.setObject(1, id);
            final ResultSet rs = statement.executeQuery();
            if (rs.next()){
                user = getUserFromResultSet(rs);
                return user;
            }
            throw new FileNotFoundException("User with id \"" + id + "\" not found");
        }
    }

    @Override
    public User findByPhone(String phone) throws FileNotFoundException, SQLException {
        User user;
        try (PreparedStatement statement = connection.prepareStatement(SQLUser.GET_BY_PHONE.QUERY)) {
            statement.setString(1, phone);
            final ResultSet rs = statement.executeQuery();
            if (rs.next()){
                user = getUserFromResultSet(rs);
                return user;
            } else {
                throw new FileNotFoundException("User with phone \"" + phone + "\" not found");
            }
        }
    }
    @Override
    public boolean update( User user) throws SQLException {
        boolean result;

        try (PreparedStatement statement = connection.prepareStatement(SQLUser.UPDATE.QUERY)) {
            statement.setString(1, user.getName());
            statement.setString(2, user.getPhone());
            statement.setObject(3, user.getId());
            result = statement.executeQuery().next();
        }
        return result;
    }

    @Override
    public boolean deleteById(UUID id) throws SQLException {
        boolean result;

        try (PreparedStatement statement = connection.prepareStatement(SQLUser.DELETE_BY_ID.QUERY)) {
            statement.setObject(1, id);
            result = statement.executeQuery().next();
        }
        return result;
    }

    private User getUserFromResultSet(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId(UUID.fromString(rs.getString("id")));
        user.setName(rs.getString("name"));
        user.setPhone(rs.getString("phone"));
        return user;
    }


    enum SQLUser {
        GET_BY_ID("SELECT id, name, phone FROM users WHERE id = (?)"),
        GET_BY_PHONE("SELECT id, name, phone FROM users WHERE phone = (?)"),
        GET_ALL("SELECT * FROM users"),
        DELETE_ALL("DELETE FROM users RETURNING id"),
        INSERT("INSERT INTO users (id, name, phone) VALUES ((?), (?), (?)) RETURNING id"),
        DELETE_BY_ID("DELETE FROM users WHERE id = (?) RETURNING id"),
        UPDATE("UPDATE users SET name = (?), phone = (?) WHERE id = (?) RETURNING id");

        String QUERY;

        SQLUser(String QUERY) {
            this.QUERY = QUERY;
        }
    }
}
