package com.repositories.json;

import com.entities.User;
import com.exceptions.NotUniqueException;
import com.repositories.interfaces.TicketRepository;
import com.repositories.interfaces.UserRepository;
import com.utilities.json.FileRepository;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class UserRepositoryImpl implements UserRepository {
    private List<User> users = new ArrayList<>();
    private File file;

    public UserRepositoryImpl(File file) throws IOException {
        this.file = file;
        users = new FileRepository<User>(file, User.class).getDataFromFile();
    }

    public UserRepositoryImpl() {
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<User> getUsers() {
        return users;
    }

    public boolean add(User user) {
        if (users.contains(user)) {
            throw new NotUniqueException("This user already contains in repository");
        }
        return users.add(user);
    }

    public boolean update(UUID id, User user) {
        for (User userFromSet : users) {
            if (userFromSet.getId().equals(id)) {
                userFromSet.setName(user.getName());
                userFromSet.setPhone(user.getPhone());
                return true;
            }
        }
        return false;
    }

    public boolean deleteByPhone(String phone) {
        return users.removeIf(user -> user.getPhone().equals(phone));
    }

    public boolean deleteById(UUID id) {
        return users.removeIf(user -> user.getId().equals(id));
    }

    public boolean update(User user) {
        return false;
    }


    public User findByPhone(String phone) throws FileNotFoundException {
        return users.stream().filter(user -> user.getPhone().equals(phone)).findFirst()
                .orElseThrow(()->new FileNotFoundException("User with phone \"" + phone + "\" not found"));
    }
    public User findById(UUID id) throws FileNotFoundException {
        return users.stream().filter(user -> user.getId().equals(id)).findFirst()
                .orElseThrow(()->new FileNotFoundException("User with id \"" + id + "\" not found"));
    }


    public void saveDataToFile() throws IOException {
        new FileRepository<User>(file, User.class).saveDataToFile(users);
    }
}
