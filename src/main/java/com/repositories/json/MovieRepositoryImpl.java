package com.repositories.json;

import com.entities.Movie;
import com.exceptions.NotUniqueException;
import com.repositories.interfaces.MovieRepository;
import com.utilities.json.FileRepository;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MovieRepositoryImpl implements MovieRepository {
    private List<Movie> movies = new ArrayList<>();
    private File file;

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    public MovieRepositoryImpl(File file) throws IOException {
        this.file = file;
        movies = new FileRepository<Movie>(file, Movie.class).getDataFromFile();
    }

    public MovieRepositoryImpl(){
    }

    public boolean deleteByTitle(String title) {
        return movies.removeIf(movie -> movie.getTitle().equals(title));
    }

    public boolean deleteById(UUID id) {
        return movies.removeIf(movie -> movie.getId().equals(id));
    }

    @Override
    public boolean update(Movie movie) {
        return false;
    }

    public boolean update(UUID id, Movie movie) {
        for (Movie movieFromSet : movies) {
            if (movieFromSet.getId().equals(id)) {
                movieFromSet.setDuration(movie.getDuration());
                movieFromSet.setGenres(movie.getGenres());
                movieFromSet.setRating(movie.getRating());
                movieFromSet.setTitle(movie.getTitle());
                return true;
            }
        }
        return false;
    }

    public List<Movie> getAllMovies() {
        return movies;
    }

    public boolean add(Movie movie) {
        if (movies.contains(movie)) {
            throw new NotUniqueException("This movie already contains in repository");
        }
        return movies.add(movie);
    }

    public Movie findByTitle(String title) throws FileNotFoundException {
        return movies.stream().filter(movie -> movie.getTitle().equals(title)).findFirst()
                .orElseThrow(()->new FileNotFoundException("Movie with title \"" + title + "\" not found"));
    }

    public Movie findById(UUID id) throws FileNotFoundException {
        return movies.stream().filter(movie -> movie.getId().equals(id)).findFirst()
                .orElseThrow(()->new FileNotFoundException("Movie with id \"" + id + "\" not found"));
    }

    public void saveDataToFile() throws IOException {
        new FileRepository<Movie>(file, Movie.class).saveDataToFile(movies);
    }

}
