package com.repositories.json;

import com.entities.Ticket;
import com.exceptions.NotUniqueException;
import com.repositories.interfaces.TicketRepository;
import com.utilities.json.FileRepository;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TicketRepositoryImpl implements TicketRepository {
    private List<Ticket> tickets = new ArrayList<>();
    private File file;

    public TicketRepositoryImpl(File file) throws IOException {
        this.file = file;
        tickets = new FileRepository<Ticket>(file, Ticket.class).getDataFromFile();
    }

    public TicketRepositoryImpl() {
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    public boolean add(Ticket ticket){
        if (tickets.contains(ticket)){
            throw new NotUniqueException("This ticket already contains in repository");
        }
        return tickets.add(ticket);
    }

    public List<Ticket> getTickets(){
        return tickets;
    }

    public Ticket findById(UUID id) throws FileNotFoundException {
        return tickets.stream().filter(ticket -> ticket.getId().equals(id)).findFirst()
                .orElseThrow(()->new FileNotFoundException("Ticket with id \"" + id + "\" not found"));
    }

    public boolean deleteById(UUID id) {
        return tickets.removeIf(ticket -> ticket.getId().equals(id));
    }

    @Override
    public boolean update(Ticket ticket) {
        return false;
    }

    public boolean update(UUID id, Ticket ticket) {
        for (Ticket ticketFromSet : tickets) {
            if (ticketFromSet.getId().equals(id)) {
                ticketFromSet.setDate(ticket.getDate());
                ticketFromSet.setMovie(ticket.getMovie());
                ticketFromSet.setPrice(ticket.getPrice());
                ticketFromSet.setUser(ticket.getUser());
                return true;
            }
        }
        return false;
    }


    public void saveDataToFile() throws IOException {
        new FileRepository<Ticket>(file, Ticket.class).saveDataToFile(tickets);
    }
}
