package com;

import com.entities.*;
import com.entities.builders.MovieBuilder;
import com.entities.builders.TicketBuilder;
import com.entities.builders.UserBuilder;
import com.repositories.database.MovieRepositoryDataBase;
import com.repositories.database.TicketRepositoryDataBase;
import com.repositories.database.UserRepositoryDataBase;
import com.services.json.MovieService;
import com.utilities.database.DataBaseConnector;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Arrays;

public class Application {

    public static void main(String[] args) {
        Movie movie = new MovieBuilder()
                .title("Fast and Furious")
                .genres(Arrays.asList(Genre.ACTION, Genre.COMEDY))
                .duration(140)
                .rating(10F)
                .build();
        Movie movie2 = new MovieBuilder()
                .title("More!!!!!!!! faster and Furious")
                .genres(Arrays.asList(Genre.ACTION, Genre.COMEDY))
                .duration(140)
                .rating(10F)
                .build();

        User user = new UserBuilder()
                .name("Ya")
                .phone("+380508210407")
                .build();
        User user2 = new UserBuilder()
                .name("Iv")
                .phone("+380538710407")
                .build();

        Ticket ticket = new TicketBuilder()
                .user(user)
                .movie(movie)
                .date(LocalDateTime.now())
                .price(1F)
                .build();
 Ticket ticket2 = new TicketBuilder()
                .user(user)
                .movie(movie2)
                .date(LocalDateTime.of(2021, 6,23,5,23))
                .price(1F)
                .build();
 Ticket ticket3 = new TicketBuilder()
                .user(user2)
                .movie(movie2)
                .date(LocalDateTime.of(2021, 6,23,5,23))
                .price(1F)
                .build();
        File file = new File ("E:\\Cinema\\src\\main\\resources\\movies.json");
        MovieService movieService = null;
        try {
            movieService = new MovieService(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        movieService.addMovie(movie);


        try {
            UserRepositoryDataBase userRepositoryDataBase = new UserRepositoryDataBase(DataBaseConnector.getConnection());
            userRepositoryDataBase.deleteAll();
            userRepositoryDataBase.add(user);
            userRepositoryDataBase.add(user2);
            MovieRepositoryDataBase movieRepositoryDataBase = new MovieRepositoryDataBase(DataBaseConnector.getConnection());
            movieRepositoryDataBase.deleteAll();
            movieRepositoryDataBase.add(movie);
            movieRepositoryDataBase.add(movie2);
            TicketRepositoryDataBase ticketRepositoryDataBase = new TicketRepositoryDataBase(DataBaseConnector.getConnection());
            ticketRepositoryDataBase.add(ticket);
            ticketRepositoryDataBase.add(ticket2);
            ticketRepositoryDataBase.add(ticket3);
            ticketRepositoryDataBase.deleteAll();
            movieRepositoryDataBase.deleteAll();
            userRepositoryDataBase.deleteAll();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
