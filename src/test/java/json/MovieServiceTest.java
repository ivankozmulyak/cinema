package json;

import com.entities.Movie;
import com.entities.builders.MovieBuilder;
import com.exceptions.NotUniqueException;
import com.services.json.MovieService;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

public class MovieServiceTest {

    private MovieService movieService;
    private Movie movie1;
    private Movie movie2;
    private String title;
    private final UUID id = UUID.fromString("e02fb260-1f0b-4a27-ba1b-bc4fbb05c28b");

    @BeforeMethod
    public void beforeMethod() throws IOException {
        File temporary = File.createTempFile("TemporaryMovie", "json",
                new File("E:\\Cinema\\src\\main\\resources"));
        movieService = new MovieService(temporary);
        title = "Film1";
        movie1 = new MovieBuilder()
                .title(title)
                .duration(120)
                .build();
        movie2 = new MovieBuilder()
                .title("Film2")
                .duration(120)
                .build();
        movie1.setId(id);
        ArrayList<Movie> movies = new ArrayList<>();
        movies.add(movie1);
        movieService.getMovieRepository().setMovies(movies);
        temporary.deleteOnExit();
    }

    @Test
    public void TestCorrectUpdate() throws FileNotFoundException {
        Assert.assertTrue(movieService.update(movie1.getId(),movie2),
                "Movies Repository does not contain movie with this Id");
        Assert.assertEquals(movieService.findById(movie1.getId()).getDuration(), movie2.getDuration(),
                "Movie1 not update field duration");
        Assert.assertEquals(movieService.findById(movie1.getId()).getTitle(), movie2.getTitle(),
                "Movie1 not update field title");
        Assert.assertEquals(movieService.findById(movie1.getId()).getGenres(), movie2.getGenres(),
                "Movie1 not update field genres");
        Assert.assertEquals(movieService.findById(movie1.getId()).getRating(), movie2.getRating(),
                "Movie1 not update field rating");

    }

    @Test
    public void TestIncorrectUpdate(){
        Assert.assertFalse(movieService.update(movie2.getId(),movie1),
                "Movies Repository contains movie with this Id");
    }

    @Test
    public void TestIncorrectDeleteById() {
        Assert.assertFalse(movieService.deleteById(UUID.randomUUID()),
                "Movies Repository contain movie with this Id");
    }


    @Test
    public void TestCorrectDeleteByTitle() {
        Assert.assertTrue(movieService.deleteByTitle(title),
                "Movies Repository does not contain movie with this title");
    }

    @Test
    public void TestIncorrectDeleteByTitle() {
        Assert.assertFalse(movieService.deleteByTitle("NotExist"),
                "Movies Repository contain movie with this title");
    }


    @Test()
    public void TestCorrectDeleteById() {
        Assert.assertTrue(movieService.deleteById(id),
                "Movies Repository does not contain movie with this Id");
    }

    @Test()
    public void TestCorrectFindByTitle() throws FileNotFoundException {
        Assert.assertEquals(movieService.findByTitle(title), movie1,
                "Movies Repository does not contain movie with this title");
    }

    @Test(expectedExceptions = FileNotFoundException.class)
    public void TestIncorrectFindByTitle() throws FileNotFoundException {
        movieService.findByTitle("NotExist");
    }

    @Test()
    public void TestCorrectAddNewMovies() throws IOException {
        movieService.addMovie(movie2);
        Assert.assertTrue(movieService.getAllMovies().contains(movie2),
                "Movies Repository does not contain movie");
    }

    @Test(expectedExceptions = NotUniqueException.class)
    public void TestIncorrectAddNewMovies() {
        movieService.addMovie(movie1);
    }


    @Test
    public void TestCorrectFindById() throws FileNotFoundException {
        Assert.assertEquals(movieService.findById(id), movie1);
    }

    @Test(expectedExceptions = FileNotFoundException.class)
    public void TestIncorrectFindById() throws FileNotFoundException {
        movieService.findById(UUID.fromString("e02fb260-1f0b-4a27-ba1b-2c4fbb05c28b"));
    }


}
