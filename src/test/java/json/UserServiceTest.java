package json;

import com.entities.User;
import com.entities.builders.UserBuilder;
import com.exceptions.NotUniqueException;
import com.services.json.UserService;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

public class UserServiceTest {


    private UserService userService;
    private User user1;
    private User user2;
    private String phone;
    private final UUID id = UUID.fromString("e02fb260-1f0b-4a27-ba1b-bc4fbb05c28b");

    @BeforeMethod
    public void beforeMethod() throws IOException {
        File temporary = File.createTempFile("TemporaryUsers", "json",
                new File("E:\\files"));
        userService = new UserService(temporary);
        phone = "+380508710407";
        user1 = new UserBuilder()
                .name("Ivan")
                .phone(phone)
                .build();
        user2 = new UserBuilder()
                .name("Ivan")
                .phone("+380518710407")
                .build();
        user1.setId(id);
        ArrayList<User> users = new ArrayList<>();
        users.add(user1);
        userService.getUserRepository().setUsers(users);
        temporary.deleteOnExit();
    }

    @Test
    public void TestCorrectUpdate() throws FileNotFoundException {
        Assert.assertTrue(userService.update(user1.getId(), user2),
                "Movies Repository does not contain movie with this Id");
        Assert.assertEquals(userService.findById(user1.getId()).getName(), user2.getName(),
                "Movie1 not update field name");
        Assert.assertEquals(userService.findById(user1.getId()).getPhone(), user2.getPhone(),
                "Movie1 not update field phone");
    }

    @Test
    public void TestIncorrectUpdate(){
        Assert.assertFalse(userService.update(user2.getId(), user1),
                "Movies Repository contains movie with this Id");
    }

    @Test
    public void TestIncorrectDeleteById() {
        Assert.assertFalse(userService.deleteById(UUID.randomUUID()),
                "Movies Repository contain movie with this Id");
    }


    @Test
    public void TestCorrectDeleteByPhone() {
        Assert.assertTrue(userService.deleteByPhone(phone),
                "Movies Repository does not contain movie with this phone");
    }

    @Test
    public void TestIncorrectDeleteByPhone() {
        Assert.assertFalse(userService.deleteByPhone("NotExist"),
                "Movies Repository contain movie with this phone");
    }


    @Test()
    public void TestCorrectDeleteById() {
        Assert.assertTrue(userService.deleteById(id),
                "Movies Repository does not contain movie with this Id");
    }

    @Test()
    public void TestCorrectFindByPhone() throws FileNotFoundException {
        Assert.assertEquals(userService.findByPhone(phone), user1,
                "Movies Repository does not contain movie with this phone");
    }

    @Test(expectedExceptions = FileNotFoundException.class)
    public void TestIncorrectFindByPhone() throws FileNotFoundException {
        userService.findByPhone("+380987654321");
    }

    @Test()
    public void TestCorrectAddNewMovies() {
        userService.addUser(user2);
        Assert.assertTrue(userService.getUsers().contains(user2),
                "Movies Repository does not contain movie");
    }

    @Test(expectedExceptions = NotUniqueException.class)
    public void TestIncorrectAddNewUsers() {
        userService.addUser(user1);
    }


    @Test
    public void TestCorrectFindById() throws FileNotFoundException {
        Assert.assertEquals(userService.findById(id), user1);
    }

    @Test(expectedExceptions = FileNotFoundException.class)
    public void TestIncorrectFindById() throws FileNotFoundException {
        userService.findById(UUID.fromString("e02fb260-1f0b-4a27-ba1b-2c4fbb05c28b"));
    }


}
