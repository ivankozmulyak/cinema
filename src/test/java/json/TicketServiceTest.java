package json;

import com.entities.*;
import com.entities.builders.MovieBuilder;
import com.entities.builders.TicketBuilder;
import com.entities.builders.UserBuilder;
import com.exceptions.NotUniqueException;
import com.services.json.TicketService;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

public class TicketServiceTest {


    private TicketService ticketService;
    private Ticket ticket1;
    private Ticket ticket2;
    private final UUID id = UUID.fromString("e02fb260-1f0b-4a27-ba1b-bc4fbb05c28b");
    private final UUID incorrectId = UUID.fromString("e02fb260-1f2b-4a27-ba1b-bc4fbb05c28b");
    LocalDate localDate;

    @BeforeMethod
    public void beforeMethod() throws IOException {
        File temporary = File.createTempFile("TemporaryTickets", "json",
                new File("E:\\files"));
        ticketService = new TicketService(temporary);
        localDate = LocalDate.of(2021, 11, 12);
        Movie movie1 = new MovieBuilder()
                .title("Film1")
                .duration(120)
                .build();
        Movie movie2 = new MovieBuilder()
                .title("Film2")
                .duration(120)
                .build();
        User user1 = new UserBuilder()
                .name("Ivan")
                .phone("+380508710407")
                .build();
        User user2 = new UserBuilder()
                .name("Ivan")
                .phone("+380518710407")
                .build();
        ticket1 = new TicketBuilder()
                .user(user1)
                .date(localDate.atTime(1, 0))
                .movie(movie1)
                .price(125.99F)
                .build();
        ticket2 = new TicketBuilder()
                .user(user2)
                .date(LocalDateTime.now())
                .movie(movie2)
                .price(125.99F)
                .build();
        ticket1.setId(id);
        ArrayList<Ticket> tickets = new ArrayList<>();
        tickets.add(ticket1);
        ticketService.getTicketRepository().setTickets(tickets);
        temporary.deleteOnExit();
    }


    @Test
    public void TestGetUsersByMovieAndDate() {
        Set<User> users = new HashSet<>();
        users.add(ticket1.getUser());
        Set<User> userList = ticketService.getUsersByMovieAndDate(ticket1.getMovie(), localDate.atTime(1, 0));
        Assert.assertEquals(users, userList);

    }


    @DataProvider(name = "data-provider-sum-of-month")
    public Object[][] DataProviderSumOfMonth() throws IOException {
        User user = new UserBuilder()
                .name("Ivan")
                .phone("+380518710407")
                .build();
        Movie movie = new MovieBuilder()
                .title("Film2")
                .duration(120)
                .build();
        Ticket ticket1 = new TicketBuilder()
                .user(user)
                .date(LocalDateTime.of(2021, 11, 11, 21, 12))
                .movie(movie)
                .price(125F)
                .build();
        Ticket ticket2 = new TicketBuilder()
                .user(user)
                .date(LocalDateTime.of(2021, 11, 11, 21, 12))
                .movie(movie)
                .price(125F)
                .build();
        Ticket ticket3 = new TicketBuilder()
                .user(user)
                .date(LocalDateTime.of(2021, 11, 11, 21, 12))
                .movie(movie)
                .price(125F)
                .build();
        Ticket ticket4 = new TicketBuilder()
                .user(user)
                .date(LocalDateTime.of(2021, 12, 11, 21, 12))
                .movie(movie)
                .price(125F)
                .build();
        Ticket ticket5 = new TicketBuilder()
                .user(user)
                .date(LocalDateTime.of(2021, 12, 11, 21, 12))
                .movie(movie)
                .price(125F)
                .build();
        Ticket ticket6 = new TicketBuilder()
                .user(user)
                .date(LocalDateTime.of(2021, 10, 11, 21, 12))
                .movie(movie)
                .price(125F)
                .build();
        List<Ticket> tickets = new ArrayList<>();
        tickets.add(ticket1);
        tickets.add(ticket2);
        tickets.add(ticket3);
        tickets.add(ticket4);
        tickets.add(ticket5);
        tickets.add(ticket6);
        File tempFile = File.createTempFile("TemporaryTickets", "json",
                new File("E:\\files"));
        TicketService ticketService = new TicketService(tempFile);
        ticketService.getTicketRepository().setTickets(tickets);
        tempFile.deleteOnExit();
        return new Object[][]{{ticketService, 2021, 12, 250F}, {ticketService, 2021, 11, 375F}, {ticketService, 2021, 10, 125F}};
    }


    @Test
    public void TestSortByCountVisits() {
        Long threshold = 1L;
        Map<Movie, Long> visitsMap = new HashMap<>();
        visitsMap.put(ticket1.getMovie(), 1L);
        Map<Movie, Long> visits = ticketService.sortByCountVisits(threshold);
        Assert.assertEquals(visitsMap, visits);
    }

    @Test(dataProvider = "data-provider-sum-of-month")
    public void TestGetMoneyForOneMonth(TicketService ticketService, int year, int month, Float sum) {
        Assert.assertEquals(ticketService.getMoneyForOneMonth(year, month), sum);
    }

    @Test
    public void TestIsTicketOnDate() {
        Assert.assertTrue(ticketService.isTicketOnDate(ticket1, this.localDate));
    }

    @Test
    public void TestGetTodayFilms() {
        Set<Movie> todayMovies = new HashSet<>();
        todayMovies.add(ticket2.getMovie());
        ticketService.addTicket(ticket2);
        Set<Movie> movieList = ticketService.getTodayFilms();
        Assert.assertEquals(todayMovies, movieList);
    }

    @Test
    public void TestCorrectUpdate() throws FileNotFoundException {
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(ticketService.update(ticket1.getId(), ticket2),
                "Ticket Repository does not contain ticket with this Id");
        Ticket ticket = ticketService.findById(id);
        softAssert.assertEquals(ticket.getMovie(), ticket2.getMovie(),
                "Ticket1 not update field movie");
        softAssert.assertEquals(ticket.getPrice(), ticket2.getPrice(),
                "Ticket1 not update field price");
        softAssert.assertEquals(ticket.getDate(), ticket2.getDate(),
                "Ticket1 not update field date");
        softAssert.assertEquals(ticket.getUser(), ticket2.getUser(),
                "Ticket1 not update field user");
        softAssert.assertAll();
    }

    @Test
    public void TestIncorrectUpdate() {
        Assert.assertFalse(ticketService.update(ticket2.getId(), ticket1),
                "Ticket Repository contains ticket with this Id");
    }

    @Test
    public void TestIncorrectDeleteById() {
        Assert.assertFalse(ticketService.deleteById(UUID.randomUUID()),
                "Ticket Repository contain ticket with this Id");
    }

    @Test
    public void TestCorrectDeleteById() {
        Assert.assertTrue(ticketService.deleteById(id),
                "Ticket Repository does not contain ticket with this Id");
    }

    @Test
    public void TestCorrectAddNewMovies() {
        ticketService.addTicket(ticket2);
        Assert.assertTrue(ticketService.getTickets().contains(ticket2),
                "Ticket Repository does not contain ticket");
    }

    @Test(expectedExceptions = NotUniqueException.class)
    public void TestIncorrectAddNewTicket() {
        ticketService.addTicket(ticket1);
    }


    @Test
    public void TestCorrectFindById() throws FileNotFoundException {
        Assert.assertEquals(ticketService.findById(id), ticket1);
    }

    @Test(expectedExceptions = FileNotFoundException.class)
    public void TestIncorrectFindById() throws FileNotFoundException {
        ticketService.findById(incorrectId);
    }


}
