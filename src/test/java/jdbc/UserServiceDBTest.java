package jdbc;

import com.entities.User;
import com.entities.builders.UserBuilder;
import com.services.database.UserServiceDB;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.*;
import java.util.UUID;

public class UserServiceDBTest {


    private UserServiceDB userService;
    private User user1;
    private User user2;
    private String phone;
    private final UUID id = UUID.fromString("e02fb260-1f0b-4a27-ba1b-bc4fbb05c28b");

    @BeforeMethod
    public void beforeMethod() throws SQLException {
        userService = new UserServiceDB();
        phone = "+380508123407";
        user1 = new UserBuilder()
                .name("Ivan")
                .phone(phone)
                .build();
        user2 = new UserBuilder()
                .name("Ivan")
                .phone("+380518123407")
                .build();
        user1.setId(id);
        userService.deleteAll();
        userService.add(user1);
    }

    @Test
    public void TestCorrectUpdate() throws FileNotFoundException, SQLException {
        Assert.assertTrue(userService.update(user1),
                "Movies Repository does not contain movie with this Id");
        Assert.assertEquals(userService.findById(user1.getId()).getName(), user1.getName(),
                "Movie1 not update field name");
        Assert.assertEquals(userService.findById(user1.getId()).getPhone(), user1.getPhone(),
                "Movie1 not update field phone");
    }
    @Test
    public void TestGetAll() throws SQLException {
        List<User> users = new ArrayList<>();
        users.add(user1);
        Assert.assertEquals(userService.getAll(), users);
    }

    @Test
    public void TestIncorrectUpdate() throws SQLException {
        Assert.assertFalse(userService.update(user2),
                "Movies Repository not contains movie with this Id");
    }

    @Test
    public void TestIncorrectDeleteById() throws SQLException {
        Assert.assertFalse(userService.deleteById(UUID.randomUUID()),
                "Movies Repository contain movie with this Id");
    }



    @Test()
    public void TestCorrectDeleteById() throws SQLException {
        Assert.assertTrue(userService.deleteById(id),
                "Movies Repository does not contain movie with this Id");
    }

    @Test()
    public void TestCorrectFindByPhone() throws FileNotFoundException, SQLException {
        Assert.assertEquals(userService.findByPhone(phone), user1,
                "Movies Repository does not contain movie with this phone");
    }

    @Test(expectedExceptions = FileNotFoundException.class)
    public void TestIncorrectFindByPhone() throws FileNotFoundException, SQLException {
        userService.findByPhone("+380987654321");
    }

    @Test
    public void TestCorrectFindById() throws FileNotFoundException, SQLException {
        Assert.assertEquals(userService.findById(id), user1);
    }

    @Test(expectedExceptions = FileNotFoundException.class)
    public void TestIncorrectFindById() throws FileNotFoundException, SQLException {
        userService.findById(UUID.fromString("e02fb260-1f0b-4a27-ba1b-2c4fbb05c28b"));
    }

    @AfterTest
    public void afterMethod() throws SQLException {
        userService.deleteAll();
    }
}
