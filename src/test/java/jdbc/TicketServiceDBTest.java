package jdbc;

import com.entities.*;
import com.entities.builders.MovieBuilder;
import com.entities.builders.TicketBuilder;
import com.entities.builders.UserBuilder;
import com.services.database.MovieServiceDB;
import com.services.database.TicketServiceDB;
import com.services.database.UserServiceDB;
import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class TicketServiceDBTest {


    private TicketServiceDB ticketService;
    private UserServiceDB userService;
    private MovieServiceDB movieService;
    private Ticket ticket1;
    private Ticket ticket2;
    private final UUID id = UUID.fromString("e02fb260-1f0b-4a27-ba1b-bc4fbb05c28b");
    private final UUID incorrectId = UUID.fromString("e02fb260-1f2b-4a27-ba1b-bc4fbb05c28b");
    LocalDate localDate;


    @BeforeMethod
    public void beforeMethod() throws SQLException {
        ticketService = new TicketServiceDB();
        userService = new UserServiceDB();
        movieService = new MovieServiceDB();
        ticketService.deleteAll();
        userService.deleteAll();
        movieService.deleteAll();
        localDate = LocalDate.of(2021, 11, 12);
        Movie movie1 = new MovieBuilder()
                .title("Film1")
                .duration(120)
                .genres(Arrays.asList(Genre.ACTION, Genre.COMEDY))
                .rating(2.4F)
                .build();
        Movie movie2 = new MovieBuilder()
                .title("Film2")
                .duration(120)
                .genres(Arrays.asList(Genre.ACTION, Genre.COMEDY))
                .rating(2.4F)
                .build();
        User user1 = new UserBuilder()
                .name("Ivan")
                .phone("+384508714407")
                .build();
        User user2 = new UserBuilder()
                .name("Ivan")
                .phone("+382218710407")
                .build();
        ticket1 = new TicketBuilder()
                .user(user1)
                .date(localDate.atTime(1, 0))
                .movie(movie1)
                .price(125.99F)
                .build();
        ticket2 = new TicketBuilder()
                .user(user2)
                .date(LocalDateTime.now())
                .movie(movie2)
                .price(125.99F)
                .build();
        ticket1.setId(id);
        userService.add(user1);
        userService.add(user2);
        movieService.add(movie1);
        movieService.add(movie2);
        ticketService.add(ticket1);

    }

    @Test
    public void TestGetAll() throws SQLException {
        List<Ticket> tickets = new ArrayList<>();
        tickets.add(ticket1);
        Assert.assertEquals(ticketService.getAll(), tickets);
    }

    @Test
    public void TestGetUsersByDate() throws SQLException {
        List<User> users = new ArrayList<>();
        users.add(ticket1.getUser());
        List<User> userList = ticketService.getUsersByDate(localDate.atTime(1, 0));
        Assert.assertEquals(users, userList);

    }
    @Test
    public void TestGetMoviesByDate() throws SQLException {
        List<Movie> movies = new ArrayList<>();
        movies.add(ticket1.getMovie());
        Assert.assertEquals(movies, ticketService.getMoviesByDate(localDate));
    }


    @Test
    public void TestGetSortedMoviesByCountOfTickets() throws SQLException {
        List<Movie> visitsList = new ArrayList<>();
        visitsList.add(ticket1.getMovie());
        List<Movie> visits = ticketService.getSortedMoviesByCountOfTickets();
        Assert.assertEquals(visitsList, visits);
    }


    @Test
    public void TestGetMoviesByCurrentDate() throws SQLException {
        List<Movie> todayMovies = new ArrayList<>();
        todayMovies.add(ticket2.getMovie());
        ticketService.add(ticket2);
        List<Movie> movieList = ticketService.getMoviesByCurrentDate();
        Assert.assertEquals(todayMovies, movieList);
    }

    @Test
    public void TestCorrectUpdate() throws FileNotFoundException, SQLException {
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(ticketService.update(ticket1),
                "Ticket Repository does not contain ticket with this Id");
        Ticket ticket = ticketService.findById(id);
        softAssert.assertEquals(ticket.getMovie(), ticket1.getMovie(),
                "Ticket1 not update field movie");
        softAssert.assertEquals(ticket.getPrice(), ticket1.getPrice(),
                "Ticket1 not update field price");
        softAssert.assertEquals(ticket.getDate(), ticket1.getDate(),
                "Ticket1 not update field date");
        softAssert.assertEquals(ticket.getUser(), ticket1.getUser(),
                "Ticket1 not update field user");
        softAssert.assertAll();
    }

    @Test
    public void TestIncorrectUpdate() throws SQLException {
        Assert.assertFalse(ticketService.update(ticket2),
                "Ticket Repository not contains ticket with this Id");
    }

    @Test
    public void TestIncorrectDeleteById() throws SQLException {
        Assert.assertFalse(ticketService.deleteById(UUID.randomUUID()),
                "Ticket Repository contain ticket with this Id");
    }

    @Test
    public void TestCorrectDeleteById() throws SQLException {
        Assert.assertTrue(ticketService.deleteById(id),
                "Ticket Repository does not contain ticket with this Id");
    }

    @Test
    public void TestCorrectFindById() throws FileNotFoundException, SQLException {
        Assert.assertEquals(ticketService.findById(id), ticket1);
    }

    @Test(expectedExceptions = FileNotFoundException.class)
    public void TestIncorrectFindById() throws FileNotFoundException, SQLException {
        ticketService.findById(incorrectId);
    }


    @AfterMethod
    public void afterMethod() throws SQLException {
        ticketService.deleteAll();
        userService.deleteAll();
        movieService.deleteAll();
    }
}
