package jdbc;

import com.entities.Genre;
import com.entities.Movie;
import com.entities.builders.MovieBuilder;
import com.services.database.MovieServiceDB;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class MovieServiceDBTest {

    private MovieServiceDB movieService;
    private Movie movie1;
    private Movie movie2;
    private final UUID id = UUID.fromString("e02fb260-1f0b-4a27-ba1b-bc4fbb05c28b");

    @BeforeMethod
    public void beforeMethod() throws SQLException {
//        Connection connection = DataBaseUtility.initConnectionWithTables(H2Connector.getConnection());
        movieService = new MovieServiceDB();
        String title = "Film1";
        movie1 = new MovieBuilder()
                .title(title)
                .duration(120)
                .genres(Arrays.asList(Genre.ACTION, Genre.COMEDY))
                .rating(2.4F)
                .build();
        movie2 = new MovieBuilder()
                .title("Film2")
                .duration(120)
                .genres(Arrays.asList(Genre.HORROR, Genre.ROMANCE, Genre.THRILLER))
                .rating(6.6F)
                .build();
        movie1.setId(id);
        movieService.deleteAll();
        movieService.add(movie1);
    }

    @Test
    public void TestGetAll() throws SQLException {
        List<Movie> movies = new ArrayList<>();
        movies.add(movie1);
        Assert.assertEquals(movieService.getAll(), movies);
    }


    @Test
    public void TestCorrectUpdate() throws FileNotFoundException, SQLException {
        movie1.setTitle("Yeah!");
        Assert.assertTrue(movieService.update(movie1),
                "Movies Repository does not contain movie with this Id");
        Assert.assertEquals(movieService.findById(movie1.getId()).getDuration(), movie1.getDuration(),
                "Movie1 not update field duration");
        Assert.assertEquals(movieService.findById(movie1.getId()).getTitle(), movie1.getTitle(),
                "Movie1 not update field title");
        Assert.assertEquals(movieService.findById(movie1.getId()).getGenres(), movie1.getGenres(),
                "Movie1 not update field genres");
        Assert.assertEquals(movieService.findById(movie1.getId()).getRating(), movie1.getRating(),
                "Movie1 not update field rating");

    }

    @Test
    public void TestIncorrectUpdate() throws SQLException {
        Assert.assertFalse(movieService.update(movie2),
                "Movies Repository not contains movie with this Id");
    }

    @Test
    public void TestIncorrectDeleteById() throws SQLException {
        Assert.assertFalse(movieService.deleteById(UUID.randomUUID()),
                "Movies Repository contain movie with this Id");
    }


    @Test()
    public void TestCorrectDeleteById() throws SQLException {
        Assert.assertTrue(movieService.deleteById(id),
                "Movies Repository does not contain movie with this Id");
    }



    @Test()
    public void TestCorrectAddNewMovies() throws SQLException {
        movieService.add(movie2);
        Assert.assertTrue(movieService.getAll().contains(movie2),
                "Movies Repository does not contain movie");
    }


    @Test
    public void TestCorrectFindById() throws FileNotFoundException, SQLException {
        Assert.assertEquals(movieService.findById(id), movie1);
    }

    @Test(expectedExceptions = FileNotFoundException.class)
    public void TestIncorrectFindById() throws FileNotFoundException, SQLException {
        movieService.findById(UUID.fromString("e02fb260-1f0b-4a27-ba1b-2c4fbb05c28b"));
    }

    @AfterTest
    public void afterMethod() throws SQLException {
        movieService.deleteAll();
    }
}
